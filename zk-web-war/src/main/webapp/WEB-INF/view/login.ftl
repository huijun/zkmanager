<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 登录</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="../../resource/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../../resource/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="../../resource/css/animate.css" rel="stylesheet">
    <link href="../../resource/css/style.css?v=4.1.0" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">h</h1>

        </div>
        <h3>欢迎使用 zkmanager</h3>

        <form class="m-t" role="form">
            <div class="form-group">
                <input type="hidden" name="client_id" id="client_id" value="${client_id[0]}"/>
                <input type="hidden" name="client_secret" id="client_secret" value="${client_secret[0]}"/>
                <input type="hidden" name="grant_type" id="grant_type" value="${grant_type[0]}"/>
                <input type="hidden" name="scope" id="scope" value="${scope[0]}"/>
                <input type="hidden" name="redirect_uri" id="redirect_uri" value="${redirect_uri[0]}"/>
                <input type="hidden" name="product" id="product" value="${product[0]}"/>
                <input type="hidden" name="appKey" id="appKey" value="${appKey[0]}"/>
                <input type="text" class="form-control" placeholder="用户名" required="" name="Username" id="Username">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="密码" required="" name="Password" id="Password">
            </div>
            <button id="loginBtn"  class="btn btn-primary block full-width m-b">登 录</button>


            <p class="text-muted text-center"> <a href="login.html#"><small>忘记密码了？</small></a> | <a href="register.html">注册一个新账号</a>
            </p>

        </form>
    </div>
</div>
<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='../assets/js/jquery.min.js'>"+"<"+"/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<script src='../assets/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
<!-- 全局js -->
<#--
<script src="../../resource/js/jquery.min.js?v=2.1.4"></script>
-->
<script src="../../resource/js/bootstrap.min.js?v=3.3.6"></script>
<!--[if lt IE 9]>
<script src="../assets/js/html5shiv.js"></script>
<script src="../assets/js/respond.min.js"></script>
<![endif]-->
<script src="../../libs/md5.js"></script>
<script src="../assets/js/ext/sha1.js"></script>
<script type="text/javascript">
    jQuery(function($) {

        $("#loginBtn").click(function (e) {
            alert("123");
            e.preventDefault();
            loginFun();
        });
        var redirectUrl = function () {
            var client_id = $("#client_id").val(),
                    client_secret = $("#client_secret").val(),
                    grant_type = $("#grant_type").val(),
                    scope = $("#scope").val(),
                    redirect_uri = $("#redirect_uri").val(),
                    product = $("#product").val(),
                    appKey = $("#appKey").val(),
                    url = "/login?client_id" + client_id +
                            "&client_secret=" + client_secret +
                            "&grant_type=" + grant_type +
                            "&scope=" + scope +
                            "&redirect_uri=" + redirect_uri + "&product=" + product + "&appKey=" + appKey;
            $.get(url, function (res) {
                console.log("res=", res);
            });
        };
        var loginFun = function () {
            alert();
            var client_id = $("#client_id").val(),
                    client_secret = $("#client_secret").val(),
                    grant_type = $("#grant_type").val(),
                    scope = $("#scope").val(),
                    redirect_uri = $("#redirect_uri").val(),
                    product = $("#product").val(),
                    appKey = $("#appKey").val(),
                    userName = $("#Username").val();
            var password = $("#Password").val();
            var encryptPwd = hex_md5(password).toLowerCase();
//        var hashObj = new jsSHA(password, "TEXT");
//        var encryptPwd = hashObj.getHash("SHA-1", "HEX");
            var url = "${oauthLoginUrl[0]}/v2/oauth/token?client_id=" + client_id +
                    "&client_secret=" + client_secret +
                    "&grant_type=" + grant_type +
                    "&scope=" + scope +
                    "&username=" + userName +
                    "&password=" + encryptPwd + "&plainPassword=" + password + "&product=" + product + "&appKey=" + appKey;
            if (userName = "") {
                alert("请输入用户名");
                return;
            }
            if (password == "") {
                alert("请输入密码");
                return;
            }
            $.ajax({
                url: url,
                type: "get",
                success: function (res) {
                    console.log("返回成功", res);
                    var redirectUri = $("#redirect_uri").val();
                    if (redirectUri.indexOf("?") > 0) {
                        redirectUri = redirectUri + "&access_token=" + res.bizData.value
                    } else {
                        redirectUri = redirectUri + "?access_token=" + res.bizData.value
                    }
                    window.location = redirectUri;
                },
                error: function (res) {
                    res = JSON.parse(res.responseText);
                    if (res.bizData.message = '坏的凭证') {
                        alert("用户不存在或密码错误")
                    }
                }
            })
        };

        function hex_md5(s){ return binl2hex(core_md5(str2binl(s), s.length * chrsz));}

    })
</script>



</body>

</html>
