/**
 * 此文件主要是写展示页面中需要对某个字段进行数据格式化的js
 * 相应的，该js对应的是数据库中video_resource_grid表中的 formatter字段
 */
/**
 * 状态格式化
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function formatExpertState(cellvalue, options, rowObject) {
    if ("1" == cellvalue) {
        return "显示";
    } else if ("2" == cellvalue) {
        return "隐藏";
    } else { //3
        return "冻结";
    }
}
/**
 * 公司名称格式化
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function formatExpertType(cellvalue, options, rowObject) {
    if ("2000" == cellvalue) {
        return "习悦";
    } else if ("2001" == cellvalue) {
        return "全通";
    }
}
/**
 * 隐藏 undefined
 * @param cellvalue
 * @returns {*}
 * @author yhwang
 */
function undefinedHide(cellvalue) {
    if (cellvalue == undefined) {
        return "";
    } else {
        return cellvalue;
    }
}
/**
 * 输入毫秒 输出 xx时xx分xx秒
 * @param cellvalue
 * @returns {string}
 * @author yhwang
 */
function formatSeconds(cellvalue) {
    var theTime = parseInt(cellvalue);// 秒
    var theTime1 = 0;// 分
    var theTime2 = 0;// 小时
    if (theTime > 60) {
        theTime1 = parseInt(theTime / 60);
        theTime = parseInt(theTime % 60);
        if (theTime1 > 60) {
            theTime2 = parseInt(theTime1 / 60);
            theTime1 = parseInt(theTime1 % 60);
        }
    }
    var result = "" + parseInt(theTime) + "秒";
    if (theTime1 > 0) {
        result = "" + parseInt(theTime1) + "分" + result;
    }
    if (theTime2 > 0) {
        result = "" + parseInt(theTime2) + "小时" + result;
    }
    return result;
}
/**
 * 日期 long 显示为 yyyy-MM-dd hh:mm:ss
 * @param cellvalue
 * @returns {string}
 */
function formatdate(cellvalue) {
    if (cellvalue == null || cellvalue == '' || cellvalue == 0) {
        return "";
    }
    var date = new Date(cellvalue);
    var month = (date.getMonth() + 1);
    var dates = date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (month < 10) {
        month = "0" + month;
    }
    if (dates < 10) {
        dates = "0" + dates;
    }
    if (hour < 10) {
        hour = "0" + hour;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return date.getFullYear() + "-" + month + "-" + dates + " " + hour + ":" + minutes + ":" + seconds;
}

//日期 long 显示为 yyyy-MM-dd
function formatshortdate(cellvalue) {
    if (cellvalue == null || cellvalue == '' || cellvalue == 0) {
        return "";
    }
    var date = new Date(cellvalue);
    var month = (date.getMonth() + 1);
    var dates = date.getDate();
    if (month < 10) {
        month = "0" + month;
    }
    if (dates < 10) {
        dates = "0" + dates;
    }
    return date.getFullYear() + "-" + month + "-" + dates;
}

function formatState(cellvalue) {
    if ("10" == cellvalue) {
        return "正常";
    } else if ("20" == cellvalue) {
        return "停用";
    } else { //30
        return "逻辑删除";
    }
}

function formatIsParent(cellvalue) {
    if ("true" == cellvalue) {
        return "是";
    } else {
        return "否"
    }
}

function formatHide(cellvalue) {
    if ("1" == cellvalue) {
        return "是";
    } else if ("0" == cellvalue) {
        return "否";
    } else { //30
        return "逻辑删除";
    }
}

function formatSex(cellvalue) {
    if ("1" == cellvalue) {
        return "男";
    } else if ("2" == cellvalue) {
        return "女";
    } else { //30
        return "未知";
    }
}

function formatMsgType(cellvalue) {
    if ("1" == cellvalue) {
        return "普通短信";
    } else if ("2" == cellvalue) {
        return "验证码";
    } else { //30
        return "未知";
    }
}

function formatTemplateType(cellvalue) {
    if ("2" == cellvalue) {
        return "验证码";
    } else if ("3" == cellvalue) {
        return "找回密码";
    } else { //30
        return "未知";
    }
}

function formatIsDefault(cellvalue) {
    if ("1" == cellvalue) {
        return "是";
    } else if ("0" == cellvalue) {
        return "否";
    } else { //30
        return "否";
    }
}
function formatEnv(cellvalue) {


    if ("1" == cellvalue) {
        return "dev";
    } else if ("2" == cellvalue) {
        return "test";
    } else if ("3" == cellvalue) {
        return "pre";
    } else if ("4" == cellvalue) {
        return "pro";
    } else { //30
        return "未知";
    }
}

function formatAssignStatus(cellvalue) {
    if ("0" == cellvalue) {
        return "正在处理";
    } else if ("1" == cellvalue) {
        return "已分配";
    } else if ("2" == cellvalue) {
        return "暂缓分配";
    } else if ("3" == cellvalue) {
        return "不可分配";
    } else { //30
        return "正在处理";
    }
}

function formatYesno(cellvalue) {
    if ("0" == cellvalue) {
        return "No";
    } else if ("1" == cellvalue) {
        return "Yes";
    } else { //30
        return "未知";
    }
}

function formatRDSConfig(cellvalue) {
    if ("1" == cellvalue) {
        return "微型A(内存1G/磁盘15G/最大连接数200/IOPS600)";
    } else if ("2" == cellvalue) {
        return "微型B(内存2G/磁盘30G/最大连接数400/IOPS1200)";
    } else if ("3" == cellvalue) {
        return "小型A(内存4G/磁盘60G/最大连接数800/IOPS2400)";
    } else if ("4" == cellvalue) {
        return "小型B(内存8G/磁盘120G/最大连接数1600/IOPS4800)";
    } else if ("5" == cellvalue) {
        return "中型(内存15G/磁盘225G/最大连接数3000/IOPS9000)";
    } else if ("6" == cellvalue) {
        return "大型(内存30G/磁盘450G/最大连接数6000/IOPS18000)";
    } else { //30
        return "未知";
    }
}

function formatRedisCpacity(cellvalue) {
    if ("1" == cellvalue) {
        return "1G";
    } else if ("2" == cellvalue) {
        return "2G";
    } else if ("4" == cellvalue) {
        return "4G";
    } else if ("8" == cellvalue) {
        return "8G";
    } else if ("16" == cellvalue) {
        return "16G";
    } else { //30
        return "未知";
    }
}
function formatOS(cellvalue) {
    if ("1" == cellvalue) {
        return "CentOS 5";
    } else if ("2" == cellvalue) {
        return "CentOS 6";
    } else if ("3" == cellvalue) {
        return "CentOS 7";
    } else if ("4" == cellvalue) {
        return "debian8.2";
    } else if ("5" == cellvalue) {
        return "fedora-20";
    } else if ("6" == cellvalue) {
        return "ubuntu 10";
    } else if ("7" == cellvalue) {
        return "ubuntu 12";
    } else if ("8" == cellvalue) {
        return "ubuntu 14";
    } else if ("9" == cellvalue) {
        return "winserver 2008";
    } else if ("10" == cellvalue) {
        return "winserver 2012";
    } else { //30
        return "未知";
    }
}

var products = null;
/**
 * 业务系统id转名称
 * @param cellvalue
 */
function formatProductName(cellvalue) {
    if (products == null) {
        $.ajax({
            url: "/admin/cmc/bizsystem/getBizSystems",
            dataType: "json",
            async: false,
            success: function (result) {
                if ("0000000" == result.rtnCode) {
                    products = result.bizData;
                }
            }
        });
    }

    for (i = 0; i < products.rows.length; i++) {
        if (cellvalue == products.rows[i].id) {
            return products.rows[i].name;
        }
    }
    return "未知";
}

var zkInfo = null;
function formatZkName(cellvalue) {
    // console.log(cellvalue);
    if (zkInfo == null) {
        $.ajax({
            url: "/admin/cmc/cmdbzkapply/getzkinfo",
            dataType: "json",
            async: false,
            success: function (result) {
                if ("0000000" == result.rtnCode) {
                    zkInfo = result.bizData;
                }
            }
        });
    }
    for (i = 0; i < zkInfo.rows.length; i++) {
         if(cellvalue == zkInfo.rows[i].id){
         return zkInfo.rows[i].name;
         }
    }
    return "未知";
}
/*function formatResourceType(cellvalue) {
 if ("1" == cellvalue) {
 return "云主机";
 } else if ("2" == cellvalue) {
 return "RDS";
 } else if ("3" == cellvalue) {
 return "Redis";
 } else { //30
 return "未知";
 }
 }*/
var resourceTypes = [];
function formatResourceType(cellvalue) {
    if (resourceTypes.length == 0) {
        $.ajax({
            url: "/admin/cmc/select/resourceType",
            dataType: 'json',
            async: false,
            success: function (data) {
                if ('0000000' == data.rtnCode) {
                    resourceTypes = data.bizData.rows;
                }
            }
        });
    }
    for (i = 0; i < resourceTypes.length; i++) {
        if (cellvalue == resourceTypes[i].id) {
            return resourceTypes[i].name;
        }
    }
    return "未知";

}